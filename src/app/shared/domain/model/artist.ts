import { Album } from "./album";

export interface Artist {
    id: number;
    name: string;
    picture: string;
    actualName?: string;
    birthday?: string;
    albums?: Album[];
}