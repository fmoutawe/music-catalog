import { Artist } from "../model/artist";

export abstract class ArtistDao {
    abstract getArtists(): Promise<Artist[]>;
    abstract getArtist(id: number): Promise<Artist>;
}
