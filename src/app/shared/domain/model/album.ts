import { Artist } from "./artist";

export interface Track {
    title: string;
    hidden: boolean;
}

export interface Album {
    id: number;
    title: string;
    cover: string;
    artist: Artist;
    tracks?: Track[];
}
