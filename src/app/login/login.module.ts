import {NgModule} from "@angular/core";
import {AuthComponent} from "./auth/auth.component";
import {LoginRoutingModule} from "./login-routing.module";
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [AuthComponent],
  imports: [LoginRoutingModule, CoreModule]
})
export class LoginModule {}
