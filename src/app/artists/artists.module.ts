import {NgModule} from "@angular/core";
import {ArtistsRoutingModule} from "./artists-routing.module";
import {ArtistsListComponent} from "./list/artists.list.component";
import {ArtistsDetailComponent} from "./detail/artists.detail.component";
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [ArtistsListComponent, ArtistsDetailComponent],
  imports: [ArtistsRoutingModule, CoreModule]
})
export class ArtistsModule {
}
