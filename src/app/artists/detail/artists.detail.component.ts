import {Component, NgZone, OnInit} from "@angular/core";
import {ArtistDao} from "../../shared/domain/dao/artist.dao";
import {ActivatedRoute} from "@angular/router";
import {Artist} from "../../shared/domain/model/artist";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'mc-artists-detail',
  templateUrl: './artists.detail.component.html'
})
export class ArtistsDetailComponent implements OnInit {
  artist: Artist;

  constructor(private artistDao: ArtistDao, private route: ActivatedRoute, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe((v) => {
      this.artistDao
        .getArtist(Number(v.artistId))
        .then((artist) => {
          this.artist = artist;
        }, () => {
          this.snackBar.open('Cet artiste n\'existe pas.', null, {duration: 2000});
        });
    })
  }
}
