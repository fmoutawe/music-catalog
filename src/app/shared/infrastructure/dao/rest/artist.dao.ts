import {ArtistDao} from '../../../domain/dao/artist.dao';
import {Artist} from '../../../domain/model/artist';
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ArtistDaoRest extends ArtistDao {

  constructor(private httpClient: HttpClient) {
    super();

  }

  getArtists(): Promise<Artist[]> {
    return this.httpClient
      .get<Artist[]>('/api/artists.json')
      .toPromise();
  }

  getArtist(id: number): Promise<Artist> {
    return this.httpClient
      .get<Artist>(`/api/artists/${id}.json`)
      .toPromise();
  }
}
