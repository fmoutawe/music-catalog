import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home.component";
import {AuthGuard} from "../core/guards/auth.guard";
import {ArtistsModule} from "../artists/artists.module";
import {AlbumsModule} from "../albums/albums.module";

const routes: Routes = [
  {
    path: '', component: HomeComponent, canActivate: [AuthGuard], children: [
      { path : '', loadChildren: 'app/artists/artists.module#ArtistsModule' },
      { path: '', loadChildren: 'app/albums/albums.module#AlbumsModule' }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class HomeRoutingModule {
}
