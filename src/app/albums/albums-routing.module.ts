import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AlbumsListComponent} from "./list/albums.list.component";
import {AlbumsDetailComponent} from "./detail/albums.detail.component";

const routes: Routes = [
  {
    path: 'albums',
    component: AlbumsListComponent,
    children: [
      {path: ':albumId', component: AlbumsDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AlbumsRoutingModule {
}
