import { Album } from "../model/album";

export abstract class AlbumDao {
    abstract getAlbums(): Promise<Album[]>;
    abstract getAlbum(id: number): Promise<Album>;
}
