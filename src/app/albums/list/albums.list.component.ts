import {Component, OnInit} from "@angular/core";
import {Album} from "../../shared/domain/model/album";
import {AlbumDao} from "../../shared/domain/dao/album.dao";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'mc-albums-list',
  templateUrl: './albums.list.component.html'
})
export class AlbumsListComponent implements OnInit {
  title: string;
  albums: Album[];

  constructor(private albumDao: AlbumDao, private snackBar: MatSnackBar) {
    this.title = 'Liste des albums';
  }

  ngOnInit(): void {
    this.albumDao.getAlbums().then(
      (albums) => this.albums = albums,
      () => this.snackBar.open('Une erreur est survenue', null, {duration: 2000}));
  }
}
