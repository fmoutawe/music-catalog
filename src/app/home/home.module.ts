import {NgModule} from "@angular/core";
import {HomeComponent} from "./home.component";
import {HomeRoutingModule} from "./home-routing.module";
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [HomeComponent],
  imports: [HomeRoutingModule, CoreModule]
})
export class HomeModule {

}
