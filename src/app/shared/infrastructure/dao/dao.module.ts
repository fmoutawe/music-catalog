import { NgModule } from "@angular/core";
import {ArtistDao} from "../../domain/dao/artist.dao";
import {ArtistDaoMock} from "./mocks/artist.dao";
import {ArtistDaoRest} from "./rest/artist.dao";
import {AlbumDao} from "../../domain/dao/album.dao";
import {AlbumDaoMock} from "./mocks/album.dao";
import {AlbumDaoRest} from "./rest/album.dao";

@NgModule({
  declarations: [],
  imports: [],
  providers: [
    { provide: ArtistDao, useClass: ArtistDaoRest },
    { provide: AlbumDao, useClass: AlbumDaoRest }
  ]
})
export class DaoRestModule {}

@NgModule({
    declarations: [],
    imports: [],
    providers: [
      { provide: ArtistDao, useClass: ArtistDaoMock },
      { provide: AlbumDao, useClass: AlbumDaoMock }
    ]
})
export class DaoMockModule {}
