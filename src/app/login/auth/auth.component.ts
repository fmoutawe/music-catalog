import {Component} from "@angular/core";
import {User} from "../../shared/domain/model/user";

@Component({
  selector: 'mc-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent {
  user: User;

  constructor() {
    this.user = {username: '', password: ''};
  }

  submit(event: Event) {
    event.preventDefault();

    console.log(this.user);
  }
}
