import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatCardModule, MatGridListModule, MatListModule, MatSidenavModule, MatSnackBarModule} from "@angular/material";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {AuthGuard} from "./guards/auth.guard";
import {NavComponent} from "./nav/nav.component";

@NgModule({
  declarations: [
    NavComponent
  ],
  providers: [
    AuthGuard
  ],
  exports: [
    CommonModule,
    FormsModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatSnackBarModule,
    RouterModule,
    NavComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatSnackBarModule,
    SharedModule
  ]
})
export class CoreModule {}
