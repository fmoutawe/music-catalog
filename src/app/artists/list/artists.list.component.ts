import {Component, OnInit} from "@angular/core";
import {ArtistDao} from "../../shared/domain/dao/artist.dao";
import {Artist} from "../../shared/domain/model/artist";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'mc-artists-list',
  templateUrl: './artists.list.component.html'
})
export class ArtistsListComponent implements OnInit {
  artists: Artist[];
  title: string = 'Liste des artistes';

  constructor(private artistDao: ArtistDao, public snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.artistDao
      .getArtists()
      .then((artists) => {
        this.artists = artists;
      }, () => {
        this.snackBar.open('Une erreur est survenue.', null, {duration: 3000});
      });
  }
}
