import {Component, OnInit} from "@angular/core";
import {AlbumDao} from "../../shared/domain/dao/album.dao";
import {MatSnackBar} from "@angular/material";
import {ActivatedRoute} from "@angular/router";
import {Album} from "../../shared/domain/model/album";

@Component({
  selector: 'mc-albums-detail',
  templateUrl: './albums.detail.component.html'
})
export class AlbumsDetailComponent implements OnInit {
  album: Album;

  constructor(private albumDao: AlbumDao, private route: ActivatedRoute, private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.route.params.subscribe((v) => {
      this.albumDao
        .getAlbum(Number(v.albumId))
        .then(
          (album) => this.album = album,
          () => this.snackBar.open('Cet album est inconnu.', null, {duration: 3000})
        );
    });
  }
}
