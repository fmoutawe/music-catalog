import {ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {ArtistsListComponent} from "./artists.list.component";
import {RouterTestingModule} from "@angular/router/testing";
import {
  MatCardModule,
  MatGridListModule,
  MatListItem,
  MatListModule,
  MatSnackBar,
  MatSnackBarModule
} from "@angular/material";
import {DaoMockModule} from "../../shared/infrastructure/dao/dao.module";
import {ArtistDao} from "../../shared/domain/dao/artist.dao";
import {Artist} from "../../shared/domain/model/artist";
import {ActivatedRoute} from "@angular/router";
import {By} from "@angular/platform-browser";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

interface TestContext {
  artists: Artist[];
  artistDao: ArtistDao;
  fixture: ComponentFixture<ArtistsListComponent>;
  component: ArtistsListComponent;
  snackBar: Partial<MatSnackBar>;
}

describe('ArtistListComponent', () => {
  beforeEach(function (this: TestContext) {
    this.artists = [{id: 0, name: 'Marc', picture: ''}, {id: 1, name: 'Jean', picture: ''}];
    this.artistDao = jasmine.createSpyObj('ArtistDao', ['getArtists']);
    this.snackBar = jasmine.createSpyObj('MatSnackBar', ['open']);

    TestBed
      .configureTestingModule({
        declarations: [ArtistsListComponent],
        providers: [
          {provide: ArtistDao, useValue: this.artistDao},
          {provide: MatSnackBar, useValue: this.snackBar}
        ],
        imports: [MatCardModule, MatListModule, MatGridListModule, MatSnackBarModule, RouterTestingModule, MatSnackBarModule, NoopAnimationsModule]
      });

    this.fixture = TestBed.createComponent(ArtistsListComponent);
    this.component = this.fixture.componentInstance;
  });

  it('should be defined', testShouldBeDefined);

  it('should have title', fakeAsync(testShoudHaveTitle));

  it ('should display fetched artists', fakeAsync(testShouldDisplayFetchedArtists));

  it ('should display error', fakeAsync(testShouldDisplayError));
});

function testShouldBeDefined(this: TestContext) {
  // GIVEN
  // WHEN
  // THEN
  expect(this.component).toBeDefined();
}

function testShoudHaveTitle(this: TestContext) {
  // GIVEN
  (this.artistDao.getArtists as jasmine.Spy).and.callFake((): Promise<Artist[]> => Promise.resolve(this.artists));
  const title = this.fixture.nativeElement.querySelector('mat-card-title');

  // WHEN
  this.fixture.detectChanges();

  // THEN
  expect(title).toBeDefined();
  expect(title.textContent).toEqual('Liste des artistes');
}

function testShouldDisplayFetchedArtists(this: TestContext) {
  // GIVEN
  (this.artistDao.getArtists as jasmine.Spy).and.callFake((): Promise<Artist[]> => Promise.resolve(this.artists));

  // WHEN
  this.fixture.detectChanges();
  tick();
  this.fixture.detectChanges();

  // GIVEN
  const listItem = this.fixture.debugElement.queryAll(By.directive(MatListItem));

  // THEN
  expect(this.artistDao.getArtists).toHaveBeenCalled();
  expect(this.component.artists).toEqual(this.artists);
  expect(this.snackBar.open).not.toHaveBeenCalled();
  expect(listItem.length).toEqual(this.artists.length);
}

function testShouldDisplayError(this: TestContext) {
  // GIVEN
  (this.artistDao.getArtists as jasmine.Spy).and.callFake((): Promise<Artist[]> => Promise.reject(''));

  // WHEN
  this.fixture.detectChanges();
  tick();
  this.fixture.detectChanges();

  // THEN
  expect(this.artistDao.getArtists).toHaveBeenCalled();
  expect(this.snackBar.open).toHaveBeenCalled();
}
