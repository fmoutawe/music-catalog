import {ArtistDao} from '../../../domain/dao/artist.dao';
import {Artist} from '../../../domain/model/artist';
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/internal/Observable";
import {of} from "rxjs/internal/observable/of";

@Injectable()
export class ArtistDaoMock extends ArtistDao {
  private artists: Artist[];

  constructor() {
    super();
    this.artists = [{
      id: 0,
      name: "John",
      picture: 'https://placekitten.com/200/300',
      actualName: "John Doe",
      birthday: "12/12/12",
      albums: []
    }, {
      id: 1,
      name: "Martin",
      picture: 'https://placekitten.com/300/300',
      actualName: "Martin Doe",
      birthday: "11/12/11",
      albums: []
    }, {
      id: 2,
      name: "François",
      picture: 'https://placekitten.com/400/300',
      actualName: "François Doe",
      birthday: "10/12/10",
      albums: []
    }]
  }

  getArtists(): Promise<Artist[]> {
    return Promise.resolve(this.artists);
  }

  getArtist(id: number): Promise<Artist> {
    return new Promise<Artist>((resolve, reject) => {
      const artist = this.artists.find((v) => v.id === id);

      return artist ? resolve(artist) : reject();
    });
  }
}
