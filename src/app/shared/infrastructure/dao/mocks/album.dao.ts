import {Injectable} from "@angular/core";
import {AlbumDao} from "../../../domain/dao/album.dao";
import {Album} from "../../../domain/model/album";

@Injectable()
export class AlbumDaoMock extends AlbumDao {
  private albums: Album[];

  constructor() {
    super();

    this.albums = [
      {
        id: 0,
        title: "Macarena",
        cover: 'http://placekitten.com/500/300',
        artist: {
          id: 0,
          name: 'Henri Des',
          picture: ''
        }
      },
      {
        id: 1,
        title: "Tutti Frutti",
        cover: 'http://placekitten.com/500/500',
        artist: {
          id: 0,
          name: 'Henri Des',
          picture: ''
        }
      }
    ]
  }

  getAlbums(): Promise<Album[]> {
    return Promise.resolve(this.albums);
  }

  getAlbum(id: number): Promise<Album> {
    return new Promise<Album>((resolve, reject) => {
      const album = this.albums.find((a) => a.id === id);

      return album ? resolve(album) : reject('');
    });
  }
}
