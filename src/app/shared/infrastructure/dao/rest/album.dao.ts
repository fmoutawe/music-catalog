import {AlbumDao} from "../../../domain/dao/album.dao";
import {Album} from "../../../domain/model/album";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class AlbumDaoRest extends AlbumDao {
  constructor (private httpClient: HttpClient) {
    super()
  }

  getAlbums(): Promise<Album[]> {
    return this.httpClient
      .get<Album[]>('/api/albums.json')
      .toPromise();
  }

  getAlbum(id: number): Promise<Album> {
    return this.httpClient
      .get<Album>(`/api/albums/${id}.json`)
      .toPromise();
  }
}
