import {fakeAsync, inject, TestBed, tick} from "@angular/core/testing";
import {HttpClient} from "@angular/common/http";
import {AlbumDaoRest} from "./album.dao";
import {HttpClientTestingBackend} from "@angular/common/http/testing/src/backend";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {AlbumDao} from "../../../domain/dao/album.dao";
import {spyOnPromise} from "@woonoz/test";

interface TestContext {
  httpClient: HttpClient;
  httpTestingController: HttpTestingController;
  albumDao : AlbumDao;
}

describe('AlbumDao', () => {
  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [AlbumDaoRest]
      });
  });

  beforeEach(inject([HttpClient, HttpTestingController, AlbumDaoRest],function (this: TestContext, httpClient, httpTestingController, albumDao) {
    this.httpClient = httpClient;
    this.httpTestingController = httpTestingController;
    this.albumDao = albumDao;
  }));


  it ('should return a album list', fakeAsync(testShouldReturnAlbumList));
  it ('should return a album list', fakeAsync(testShouldReturnError));
});

function testShouldReturnAlbumList(this: TestContext) {
  // GIVEN

  // WHEN
  const result = spyOnPromise(this.albumDao.getAlbums());

  // THEN
  const req = this.httpTestingController.expectOne('/api/albums.json');

  req.flush([]);

  tick();

  expect(result.then).toHaveBeenCalled();
  expect(req.request.method).toEqual('GET');

  this.httpTestingController.verify();

}

function testShouldReturnError(this: TestContext) {
  // GIVEN

  // WHEN
  const result = spyOnPromise(this.albumDao.getAlbums());

  // THEN
  const req = this.httpTestingController.expectOne('/api/albums.json');


  req.error(new ErrorEvent(''));

  tick();

  expect(req.request.method).toEqual('GET');
  expect(result.catch).toHaveBeenCalled();

  this.httpTestingController.verify();

}
