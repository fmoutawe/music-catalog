import {ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {ArtistsDetailComponent} from "./artists.detail.component";
import {DaoMockModule} from "../../shared/infrastructure/dao/dao.module";
import {MatCardModule, MatGridListModule, MatListModule, MatSnackBar, MatSnackBarModule} from "@angular/material";
import {ActivatedRoute, Params} from "@angular/router";
import {of} from "rxjs/internal/observable/of";
import {ArtistDao} from "../../shared/domain/dao/artist.dao";
import {Artist} from "../../shared/domain/model/artist";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {Subject} from "rxjs/internal/Subject";

interface TestContext {
  artist: Artist;
  artistDao: ArtistDao;
  params: Subject<Params>;
  fixture: ComponentFixture<ArtistsDetailComponent>;
  component: ArtistsDetailComponent;
  snackBar: Partial<MatSnackBar>;
}

describe('ArtistsDetailComponent', () => {
  it('should be defined', testShouldBeDefined);

  it('should display fetched artist for valid id', fakeAsync(testShouldDisplayFetchedArtistForValidId));

  it('should display error for invalid id', fakeAsync(testShouldDisplayErrorForInvalidId));

  beforeEach(function (this: TestContext) {
    this.params = new Subject<Params>();

    this.artist = {id: 0, name: 'Marc', picture: ''};
    this.artistDao = jasmine.createSpyObj('ArtistDao', ['getArtist']);
    this.snackBar = jasmine.createSpyObj('MatSnackBar', ['open']);

    TestBed
      .configureTestingModule({
        declarations: [ArtistsDetailComponent],
        providers: [
          {provide: ActivatedRoute, useValue: { params: this.params }},
          {provide: ArtistDao, useValue: this.artistDao},
          {provide: MatSnackBar, useValue: this.snackBar}
        ],
        imports: [MatCardModule, MatListModule, MatGridListModule, MatSnackBarModule, NoopAnimationsModule]
      });

    this.fixture = TestBed.createComponent(ArtistsDetailComponent);
    this.component = this.fixture.componentInstance;
  });

});

function testShouldBeDefined(this: TestContext) {
  // GIVEN
  // WHEN
  // THEN
  expect(this.component).toBeDefined();
}

function testShouldDisplayFetchedArtistForValidId(this: TestContext) {
  // GIVEN
  const validArtistId = 0;
  const title = this.fixture.nativeElement.querySelector('mat-card-title');
  (this.artistDao.getArtist as jasmine.Spy).and.callFake((): Promise<Artist> => Promise.resolve(this.artist));

  // WHEN
  this.fixture.detectChanges();
  this.params.next({ artistId: validArtistId });
  tick();
  this.fixture.detectChanges();

  // THEN
  expect(this.artistDao.getArtist).toHaveBeenCalled();
  expect(this.artistDao.getArtist).toHaveBeenCalledWith(validArtistId);
  expect(title.textContent).toBe(this.artist.name);
  expect(this.snackBar.open).not.toHaveBeenCalled();
}

function testShouldDisplayErrorForInvalidId(this: TestContext) {
  // GIVEN
  const invalidArtistId = -1;
  const title = this.fixture.nativeElement.querySelector('mat-card-title');
  (this.artistDao.getArtist as jasmine.Spy).and.callFake((): Promise<Artist> => Promise.reject(null));

  // WHEN
  this.fixture.detectChanges();

  this.params.next({ artistId: invalidArtistId });

  tick();

  // THEN
  expect(this.artistDao.getArtist).toHaveBeenCalled();
  expect(this.artistDao.getArtist).toHaveBeenCalledWith(invalidArtistId);
  expect(this.component.artist).toBeUndefined();
  expect(title.textContent).toBe('');
  expect(this.snackBar.open).toHaveBeenCalled();
}
