import {RouterModule, Routes} from "@angular/router";
import {ArtistsListComponent} from "./list/artists.list.component";
import {ArtistsDetailComponent} from "./detail/artists.detail.component";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: 'artists',
    component: ArtistsListComponent,
    children: [
      { path: ':artistId', component: ArtistsDetailComponent }
    ]
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ArtistsRoutingModule {}
