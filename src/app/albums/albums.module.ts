import {NgModule} from "@angular/core";
import {AlbumsListComponent} from "./list/albums.list.component";
import {AlbumsRoutingModule} from "./albums-routing.module";
import {AlbumsDetailComponent} from "./detail/albums.detail.component";
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [AlbumsListComponent, AlbumsDetailComponent],
  imports: [AlbumsRoutingModule, CoreModule]
})
export class AlbumsModule {}
