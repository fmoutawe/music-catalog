import { NgModule } from "@angular/core";
import {DaoMockModule, DaoRestModule} from "./infrastructure/dao/dao.module";
import {environment} from "../../environments/environment";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    declarations: [],
    imports: [environment.mock ? DaoMockModule : DaoRestModule,  HttpClientModule ],
    providers: [],
    bootstrap: []
})
export class SharedModule {}
